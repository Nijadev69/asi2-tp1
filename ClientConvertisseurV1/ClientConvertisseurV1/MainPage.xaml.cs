﻿using ClientConvertisseurV1.Model;
using ClientConvertisseurV1.Service;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// Pour plus d'informations sur le modèle d'élément Page vierge, consultez la page https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace ClientConvertisseurV1
{
    /// <summary>
    /// Une page vide peut être utilisée seule ou constituer une page de destination au sein d'un frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public MainPage()
        {
            this.InitializeComponent();
            ActionGetData();
        }

        private async void ActionGetData()
        {
            var result = await WSService.GetInstance().GetAllDevisesAsync();
            this.comboBoxCurrency.DataContext = new List<Devise>(result);
        }

        private void buttonConvert_Click(object sender, RoutedEventArgs e)
        {
            double amount;

            if(double.TryParse(textBoxAmount.Text, out amount))
            {
                Devise devise = (Devise)comboBoxCurrency.SelectedItem;

                double convertedAmount = amount * devise.Taux;
                textBoxConvertedAmount.Text = convertedAmount.ToString();
            }
            else
            {
                showMessage("Erreur dans le montant, veuillez utiliser ',' comme séparateur");
            }
        }

        private async void showMessage(string msg)
        {
            MessageDialog msgDial = new MessageDialog(msg);
            await msgDial.ShowAsync();
        }
    }
}
