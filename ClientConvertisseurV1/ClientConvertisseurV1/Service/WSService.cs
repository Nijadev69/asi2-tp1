﻿using ClientConvertisseurV1.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ClientConvertisseurV1.Service
{
    class WSService
    {
        private static HttpClient client;
        private static WSService instance = null;

        private WSService()
        {
            
        }

        public static WSService GetInstance()
        {
            if(instance == null)
            {
                instance = new WSService();
                instance.init();
            }

            return instance;
        }

        private void init()
        {
            client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:5000/api/");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<List<Devise>> GetAllDevisesAsync()
        {
            List<Devise> devises = null;

            HttpResponseMessage response = await client.GetAsync("devise");
            if (response.IsSuccessStatusCode)
            {
                devises = await response.Content.ReadAsAsync<List<Devise>>();
            }

            return devises;
        }

    }
}
